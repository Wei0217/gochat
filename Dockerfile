FROM golang:1.20.7-alpine
WORKDIR /GoChat
COPY . /GoChat
RUN go mod download \
    && go build -o /GoChat/server/main/serverrun /GoChat/server/main/main.go
EXPOSE 8080
CMD ["/GoChat/server/main/serverrun"]