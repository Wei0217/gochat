package model

import "encoding/json"

type User struct {
	UserId   uint   `json:"userId"`   // Unique in Redis(hash field)
	UserName string `json:"userName"` // Unique in Redis(hash value)
	UserPwd  string `json:"userPwd"`  // (hash value)
}

func NewUser(uId uint, uName string, uPwd string) *User {
	newUser := User{uId, uName, uPwd}
	return &newUser
}

func (this *User) Serialize() ([]byte, error) {
	return json.Marshal(this)
}

func DeserializeUser(data []byte) (*User, error) {
	var userData User
	err := json.Unmarshal(data, &userData)
	if err != nil {
		return nil, err
	}
	return &userData, nil
}
