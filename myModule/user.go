package myModule

// import "encoding/json"

// type user struct {
// 	UserId   uint   `json:"userId"`   // Unique in Redis(hash field)
// 	UserName string `json:"userName"` // Unique in Redis(hash value)
// 	Password string `json:"password"` // (hash value)
// }

// func NewUser(uId uint, uName string, pwd string) *user {
// 	newUser := user{uId, uName, pwd}
// 	return &newUser
// }

// func (this *user) Serialize() ([]byte, error) {
// 	return json.Marshal(this)
// }

// func DeserializeUser(data []byte) (*user, error) {
// 	var userData user
// 	err := json.Unmarshal(data, &userData)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &userData, nil
// }
