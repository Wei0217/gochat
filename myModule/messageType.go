package myModule

import (
	"encoding/json"
)

type ResCode struct {
	Code uint16
	Body []byte
}

func (r *ResCode) Serialize() ([]byte, error) {
	return json.Marshal(r)
}

func DeserializeResCode(data []byte) (*ResCode, error) {
	var resCode ResCode
	err := json.Unmarshal(data, &resCode)
	if err != nil {
		return nil, err
	}
	return &resCode, nil
}

// type LoginMes struct {
// 	User model.User `json:"user"`
// }

// type LoginMesRes struct {
// 	Code uint16 `json:"code"`
// 	Body []byte `json:"body"`
// }
// type RegisterMes struct {
// 	User model.User `json:"user"`
// }

// type RegisterMesRes struct {
// 	Code uint16 `json:"code"`
// 	Body []byte `json:"body"`
// }
