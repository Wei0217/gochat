package myModule

import (
	"encoding/json"
	"fmt"
	"net"
	"time"
)

type MsgType string
type Request string

// type MyToken string

const (
	// Msg_FirstConnect MsgType = "Msg_FirstConnect"
	Msg_UserLogin      MsgType = "Msg_UserLogin"
	Msg_UserRegister   MsgType = "Msg_UserRegister"
	Msg_Exit           MsgType = "Msg_Exit"
	Msg_CreateSomeUser MsgType = "Msg_CreateSomeUser"
	Msg_GetOnlineUsers MsgType = "Msg_GetOnlineUsers"
	Msg_Broadcast      MsgType = "Msg_Broadcast"
	Msg_PrivateChat    MsgType = "Msg_PrivateChat"

	Nak Request = "Nak"
	Ack Request = "Ack"
	Req Request = "Req"

	Success            uint16 = 200 // 作業成功
	UserLoginFail      uint16 = 300 // Login錯誤
	UserRegisterFail   uint16 = 400 // Register錯誤
	CreateSomeUserFail uint16 = 404 // 事先製造DB資料錯誤
	PrivateChatFail    uint16 = 500 // 事先製造DB資料錯誤
)

type Message struct {
	// Token     MyToken   `json:"token"`     // 每個連線都有唯一的token，暫時讓token=userID、Guest0~...
	MsgType   MsgType   `json:"msgType"`   // 要處理的業務(處理的cmd種類)
	ReqType   Request   `json:"reqType"`   // Nak, Ack, Req 處理命令時用
	Content   []byte    `json:"content"`   // Data
	Timestamp time.Time `json:"timestamp"` // 發送時間
	TranId    uint8     `json:"tranId"`    // messageId 用來比較是不是超時的信息，超時就不處理
	// Sender    string    `json:"sender"`
	// Recipient string    `json:"recipient"`
}

func (this *Message) Serialize() ([]byte, error) {
	return json.Marshal(this)
}

func DeserializeMessage(data []byte) (*Message, error) {
	var message Message
	err := json.Unmarshal(data, &message)
	if err != nil {
		return nil, err
	}
	return &message, nil
}

func SendMessageToRemote(msg *Message, conn net.Conn) error {
	sendMsg, err := msg.Serialize()
	if err != nil {
		fmt.Println("SendMessageToRemote Serialize err =", err)
		return err
	}
	// 發送訊息給服務器
	_, err = conn.Write([]byte(sendMsg))
	if err != nil {
		fmt.Println("SendMessageToRemote Write err =", err)
		return err
	}
	return nil
}

func ReadMessageFromRemote(conn net.Conn) (*Message, error) {
	buffer := make([]byte, 1024)
	n, err := conn.Read(buffer) // 賭塞
	if err != nil {
		fmt.Println("ReadMessageFromRemote Read err =", err)
		return nil, err
	}

	message, err := DeserializeMessage(buffer[:n])
	if err != nil {
		fmt.Println("ReadMessageFromRemote DeserializeMessage err = ", err)
		return nil, err
	}
	return message, nil
}
