package myModule

import "encoding/json"

type PrivateChat struct {
	UserName string `json:"userName"`
	ChatMsg  string `json:"chatMsg"`
}

func (this *PrivateChat) Serialize() ([]byte, error) {
	return json.Marshal(this)
}

func DeserializePrivateChat(data []byte) (*PrivateChat, error) {
	var privateChat PrivateChat
	err := json.Unmarshal(data, &privateChat)
	if err != nil {
		return nil, err
	}
	return &privateChat, nil
}
