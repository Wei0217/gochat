package myModule

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func TrimNewline(str string) string {
	str = strings.ReplaceAll(str, "\r", "")
	str = strings.ReplaceAll(str, "\n", "")
	return str
}

func GetExePath() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exePath := filepath.Dir(ex)
	fmt.Println("exePath:", exePath)
	return exePath
}
