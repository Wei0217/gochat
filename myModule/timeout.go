package myModule

import (
	"errors"
	"fmt"
	"time"
)

var ResTimeout chan int8
var TranId uint8 // 每筆transaction的都更新，用來辨識當前收到的message是不是上一次交易timeout的，從1開始，0有特殊意義(server主動給的msg)

func MainCheckTimeout(second int) error {
	// 堵塞，直到成功，或second秒timeout
	select {
	case <-time.After(time.Duration(second) * time.Second):
		UpdateTranId()
		fmt.Printf("main 超時")
		return errors.New("MainCheckTimeout timeout")
	case <-ResTimeout:
		UpdateTranId()
		success := <-ResTimeout
		// fmt.Println("success =", success)
		if success == 1 {
			fmt.Println("MainCheckTimeout success response")
			return nil
		} else if success == -1 {
			return errors.New("MainCheckTimeout Fail response")
		} else {
			fmt.Printf("MainCheckTimeout success = %v\n", success)
			return errors.New("MainCheckTimeout unknown success value")
		}
	}
}

func GoroutineCheckTimeout(tranId uint8) error {
	// message的tranId=0，表示是server主動發的，不用進行timeout判定
	if tranId == 0 {
		return nil
	}

	// tranId not match 表示主線程已進入下個循環，故不處理此timeout的message
	if tranId != TranId {
		return errors.New(fmt.Sprintf("Suppose tranId = %v, but %v", TranId, tranId))
	}

	select {
	case ResTimeout <- 0: // 第一次send，讓main知道信息回來且沒超時，main等待第二次send的操作是否成功
		fmt.Println("收到回應，未超時")
		return nil
		// 未超時，所以繼續執行下方程式碼，必須再send一次 ResTimeout
	case <-time.After(0): // 讓main知道信息回來且沒超時
		fmt.Println("收到回應，已超時")
		return errors.New("該回應已超時")
	}
}

func UpdateTranId() {
	TranId = TranId%100 + 1
	// TranId範圍 => [1, 100]
}
