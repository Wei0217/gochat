package myModule

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"time"

	"dennis.com/goChat/model"
)

type ModeType string

const (
	Mode_Guest       ModeType = "Mode_Guest"       // 未登入
	Mode_Login       ModeType = "Mode_Login"       // 登入狀態
	Mode_PrivateChat ModeType = "Mode_PrivateChat" // 私聊狀態
	Mode_MultiChat   ModeType = "Mode_MultiChat"   // 群聊狀態
)

func (m ModeType) String() string {
	return string(m)
}

type Client struct {
	Conn     net.Conn
	UserId   uint     // unique	目前沒有唯一機制
	UserName string   // uniqe map_key
	Mode     ModeType // 目前Client的狀態
	// Token  MyToken
	// Password string   // 之後hash存DB
}

func (this Client) Login() error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter UserName: ")
	userName, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Login userName err =", err)
		return err
	}
	fmt.Print("Enter Password: ")
	userPwd, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Login password err =", err)
		return err
	}

	userName = TrimNewline(userName)
	userPwd = TrimNewline(userPwd)
	fmt.Printf("%q", userPwd)
	user, err := model.NewUser(0, userName, userPwd).Serialize()
	if err != nil {
		fmt.Println("Login NewUser err =", err)
		return err
	}
	fmt.Println(string(user))
	msg := Message{
		// Token:     this.Token,
		MsgType:   Msg_UserLogin,
		ReqType:   Ack,
		Content:   user,
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("Login TranId =", TranId)

	err = SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("Login SendMessageToRemote err =", err)
		return err
	}

	// sendMsg, err := msg.Serialize()
	// if err != nil {
	// 	fmt.Println("Login msg.Serialize err =", err)
	// 	return err
	// }
	// // 發送訊息給服務器
	// _, err = this.Conn.Write([]byte(sendMsg))
	// if err != nil {
	// 	fmt.Println("Login Write err =", err)
	// 	return err
	// }
	return nil
}

func (this Client) Register() error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter UserName: ")
	userName, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Register userName err =", err)
		return err
	}
	fmt.Print("Enter Password: ")
	userPwd, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Register password err =", err)
		return err
	}

	userName = TrimNewline(userName)
	userPwd = TrimNewline(userPwd)
	fmt.Printf("%q", userPwd)
	user, err := model.NewUser(0, userName, userPwd).Serialize()
	if err != nil {
		fmt.Println("Register NewUser err =", err)
		return err
	}
	fmt.Println(string(user))
	msg := Message{
		// Token:     this.Token,
		MsgType:   Msg_UserRegister,
		ReqType:   Ack,
		Content:   user,
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("Register TranId =", TranId)

	err = SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("Register SendMessageToRemote err =", err)
		return err
	}
	return nil
}

func (this Client) GetOnlineUsers() error {
	msg := Message{
		MsgType:   Msg_GetOnlineUsers,
		ReqType:   Ack,
		Content:   nil,
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("GetOnlineUsers TranId =", TranId)

	err := SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("GetOnlineUsers SendMessageToRemote err =", err)
		return err
	}
	return nil
}

func (this Client) Broadcast() error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Say something: ")
	saySome, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Broadcast ReadString err =", err)
		return err
	}

	saySome = TrimNewline(saySome)

	msg := Message{
		MsgType:   Msg_Broadcast,
		ReqType:   Ack,
		Content:   []byte(saySome),
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("Broadcast TranId =", TranId)

	err = SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("Broadcast SendMessageToRemote err =", err)
		return err
	}
	return nil
}

func (this Client) PrivateChat() error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Chat to whom(UserId): ")
	reciepient, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("PrivateChat reciepient ReadString err =", err)
		return err
	}
	reciepient = TrimNewline(reciepient)

	fmt.Print("Say something: ")
	saySome, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("PrivateChat saySome ReadString err =", err)
		return err
	}
	saySome = TrimNewline(saySome)

	data, err := (&PrivateChat{reciepient, saySome}).Serialize()
	if err != nil {
		fmt.Println("PrivateChat data Serialize err =", err)
		return err
	}
	msg := Message{
		MsgType:   Msg_PrivateChat,
		ReqType:   Ack,
		Content:   data,
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("PrivateChat TranId =", TranId)

	err = SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("PrivateChat SendMessageToRemote err =", err)
		return err
	}
	return nil
}

func (this Client) CreateSomeUsers() error {
	msg := Message{
		// Token:     this.Token,
		MsgType:   Msg_CreateSomeUser,
		ReqType:   Ack,
		Content:   nil,
		Timestamp: time.Now(),
		TranId:    TranId,
	}
	fmt.Println("CreateSomeUsers TranId =", TranId)

	err := SendMessageToRemote(&msg, this.Conn)
	if err != nil {
		fmt.Println("CreateSomeUsers SendMessageToRemote err =", err)
		return err
	}
	return nil
}
