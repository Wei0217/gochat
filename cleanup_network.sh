#!/bin/bash

# 檢查是否不存在名稱為 "dennisnet" 的網路
if ! [ "$(docker network ls -q -f name=dennisnet)" ]; then
  # 創建 "dennisnet" 網路
  docker network create dennisnet
fi