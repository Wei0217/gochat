package control

import (
	"errors"
	"fmt"
	"net"
	"time"

	"dennis.com/goChat/model"
	"dennis.com/goChat/myModule"
)

func UserLogin(message *myModule.Message, client *myModule.Client) error {
	user, err := model.DeserializeUser(message.Content)
	if err != nil {
		fmt.Println("UserLogin err = ", err)
	}
	// 連redis
	loginUser, err := model.MyUserDao.Login(user.UserName, user.UserPwd)
	if err != nil {
		fmt.Println("UserLogin MyUserDao.Login err =", err)
		// fmt.Println("密碼錯誤")
		resCode := myModule.ResCode{Code: myModule.UserLoginFail, Body: []byte(err.Error())}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("密碼錯誤 UserLogin resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_UserLogin,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			Timestamp: time.Now(),
			TranId:    message.TranId,
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("密碼錯誤 UserLogin SendMessageToRemote err =", err)
			return err
		}
	} else {
		// fmt.Println("密碼正確")
		body, err := loginUser.Serialize()
		if err != nil {
			fmt.Println("密碼正確 UserLogin loginUser Serialize err =", err)
			return err
		}
		resCode := myModule.ResCode{Code: myModule.Success, Body: body}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("密碼正確 UserLogin resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_UserLogin,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			TranId:    message.TranId,
			Timestamp: time.Now(),
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("密碼正確 UserLogin SendMessageToRemote err =", err)
			return err
		}
		// 登入用戶，加入map Clients裡，方便管理
		client.UserId = loginUser.UserId
		client.UserName = loginUser.UserName
		client.Mode = myModule.Mode_Login
		MyOnlineClientMgr.AddOnlineClient(client)
		// LoginClients[loginUser.UserName] = client
	}
	return nil
}

func UserRegister(message *myModule.Message, client *myModule.Client) error {
	user, err := model.DeserializeUser(message.Content)
	if err != nil {
		fmt.Println("UserRegister err = ", err)
		return err
	}
	// 連redis
	RegisterUser, err := model.MyUserDao.Register(user.UserId, user.UserName, user.UserPwd)
	if err != nil {
		fmt.Println("UserRegister MyUserDao.Register err =", err)
		// fmt.Println("註冊失敗")
		resCode := myModule.ResCode{Code: myModule.UserRegisterFail, Body: []byte(err.Error())}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("註冊失敗 UserRegister resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_UserRegister,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			Timestamp: time.Now(),
			TranId:    message.TranId,
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("註冊失敗 UserRegister SendMessageToRemote err =", err)
			return err
		}
	} else {
		// fmt.Println("註冊成功")
		body, err := RegisterUser.Serialize()
		if err != nil {
			fmt.Println("註冊成功 UserRegister Serialize err =", err)
			return err
		}
		resCode := myModule.ResCode{Code: myModule.Success, Body: body}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("註冊成功 UserRegister resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_UserRegister,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			TranId:    message.TranId,
			Timestamp: time.Now(),
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("註冊成功 UserRegister SendMessageToRemote err =", err)
			return err
		}
		// 註冊成功後，直接登入，加入map Clients裡
		client.UserId = RegisterUser.UserId
		client.UserName = RegisterUser.UserName
		client.Mode = myModule.Mode_Login
		MyOnlineClientMgr.AddOnlineClient(client)
		// LoginClients[RegisterUser.UserName] = client
	}
	return nil
}

func CreateSomeUser(message *myModule.Message, client *myModule.Client) error {
	// 連redis
	err := model.MyUserDao.CreateSomeUser()
	if err != nil {
		fmt.Println("CreateSomeUser MyUserDao.CreateSomeUser err =", err)
		// fmt.Println("失敗")
		resCode := myModule.ResCode{Code: myModule.CreateSomeUserFail, Body: []byte(err.Error())}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("失敗 CreateSomeUser resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_CreateSomeUser,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			Timestamp: time.Now(),
			TranId:    message.TranId,
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("失敗 CreateSomeUser SendMessageToRemote err =", err)
			return err
		}
	} else {
		// fmt.Println("註冊成功")
		resCode := myModule.ResCode{Code: myModule.Success, Body: nil}
		resCodeData, err := resCode.Serialize()
		if err != nil {
			fmt.Println("成功 CreateSomeUser resCode Serialize err =", err)
			return err
		}
		msg := myModule.Message{
			// Token:     myModule.MyToken(user.UserName),
			MsgType:   myModule.Msg_CreateSomeUser,
			ReqType:   myModule.Req,
			Content:   resCodeData,
			TranId:    message.TranId,
			Timestamp: time.Now(),
		}
		err = myModule.SendMessageToRemote(&msg, client.Conn)
		if err != nil {
			fmt.Println("成功 CreateSomeUser SendMessageToRemote err =", err)
			return err
		}
	}
	return nil
}

func GetOnlineUsers(message *myModule.Message, client *myModule.Client) error {
	data, err := MyOnlineClientMgr.SerializeSlice()
	if err != nil {
		fmt.Println("GetOnlineUsers SerializeSlice err =", err)
		return err
	}
	resCode := myModule.ResCode{Code: myModule.Success, Body: data}
	resCodeData, err := resCode.Serialize()
	if err != nil {
		fmt.Println("GetOnlineUsers resCode Serialize err =", err)
		return err
	}
	msg := myModule.Message{
		MsgType:   myModule.Msg_GetOnlineUsers,
		ReqType:   myModule.Req,
		Content:   resCodeData,
		TranId:    message.TranId,
		Timestamp: time.Now(),
	}
	err = myModule.SendMessageToRemote(&msg, client.Conn)
	if err != nil {
		fmt.Println("GetOnlineUsers SendMessageToRemote err =", err)
		return err
	}
	return nil
}

func BroadcastOnlineUser(message *myModule.Message, client *myModule.Client) (err error) {
	sendStr := []byte(client.UserName + ": " + string(message.Content))
	resCode := myModule.ResCode{Code: myModule.Success, Body: sendStr} // message.Content = []byte("saySome")
	resCodeData, err := resCode.Serialize()
	if err != nil {
		fmt.Println("BroadcastOnlineUser resCode Serialize err =", err)
		return
	}
	msg := myModule.Message{
		MsgType:   myModule.Msg_Broadcast,
		ReqType:   myModule.Ack,
		Content:   resCodeData,
		TranId:    0, // 此msg為server主動發，所以為0
		Timestamp: time.Now(),
	}

	var errUsers string
	for _, onlineClient := range MyOnlineClientMgr.OnlineClients {
		err = myModule.SendMessageToRemote(&msg, onlineClient.Conn)
		if err != nil {
			fmt.Printf("BroadcastOnlineUser SendMessageToRemote %v err =%v\n", onlineClient.UserName, err)
			errUsers += onlineClient.UserName + ","
		}
	}
	if errUsers != "" {
		err = errors.New(errUsers + "SendMessage Fail.")
	}
	return
}

func PrivateChatOnlineUser(message *myModule.Message, client *myModule.Client) (err error) {
	privateChat, err := myModule.DeserializePrivateChat(message.Content)
	if err != nil {
		fmt.Println("PrivateChatOnlineUser DeserializePrivateChat err =", err)
		err = SendResponseMessage(myModule.PrivateChatFail, []byte(err.Error()), myModule.Msg_PrivateChat, myModule.Req, message.TranId, client.Conn)
		if err != nil {
			fmt.Println("PrivateChatOnlineUser 1 SendResponseMessage err =", err)
			return
		}
		return
	}

	_, err = model.MyUserDao.GetUserByUserName(privateChat.UserName)
	if err != nil {
		fmt.Println("PrivateChatOnlineUser GetUserByUserName err =", err)
		err = SendResponseMessage(myModule.PrivateChatFail, []byte(err.Error()), myModule.Msg_PrivateChat, myModule.Req, message.TranId, client.Conn)
		if err != nil {
			fmt.Println("PrivateChatOnlineUser 2 SendResponseMessage err =", err)
			return
		}
		return
	}

	reciepientClient, exist := MyOnlineClientMgr.OnlineClients[privateChat.UserName]
	if !exist {
		err = errors.New("該用戶不在線")
		err = SendResponseMessage(myModule.PrivateChatFail, []byte(err.Error()), myModule.Msg_PrivateChat, myModule.Req, message.TranId, client.Conn)
		if err != nil {
			fmt.Println("PrivateChatOnlineUser 3 SendResponseMessage err =", err)
			return
		}
		return
	}

	sendStr := client.UserName + ": " + privateChat.ChatMsg
	err = SendResponseMessage(myModule.Success, []byte(sendStr), myModule.Msg_PrivateChat, myModule.Ack, 0, reciepientClient.Conn)
	if err != nil {
		fmt.Println("PrivateChatOnlineUser 4 SendResponseMessage err =", err)
		return
	}

	err = SendResponseMessage(myModule.Success, nil, myModule.Msg_PrivateChat, myModule.Req, message.TranId, client.Conn)
	if err != nil {
		fmt.Println("PrivateChatOnlineUser 5 SendResponseMessage err =", err)
		return
	}

	return
}

func SendResponseMessage(code uint16, body []byte, msgType myModule.MsgType, reqType myModule.Request, trandId uint8, conn net.Conn) error {
	resCode := myModule.ResCode{Code: code, Body: body}
	resCodeData, err := resCode.Serialize()
	if err != nil {
		fmt.Println("responseMessage resCode Serialize err =", err)
		return err
	}
	message := myModule.Message{
		MsgType:   msgType,
		ReqType:   reqType,
		Content:   resCodeData,
		TranId:    trandId,
		Timestamp: time.Now(),
	}

	err = myModule.SendMessageToRemote(&message, conn)
	if err != nil {
		fmt.Println("responseMessage SendMessageToRemote err =", err)
		return err
	}

	return nil
}
