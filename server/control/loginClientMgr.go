package control

import (
	"encoding/json"

	"dennis.com/goChat/myModule"
)

// map 是線程不安全的，雖然UserName唯一，僅自己登入時能操作map，較無問題
// 但目前也沒有考慮，同時有同樣用戶名註冊
var (
	MyOnlineClientMgr OnlineClientMgr
)

type OnlineClientMgr struct {
	OnlineClients      map[string]*myModule.Client
	OnlineClientsSlice []string
	OnlineClientsNum   uint
}

func InitOnlineClientMgr(initClientNum int) {
	MyOnlineClientMgr = OnlineClientMgr{
		OnlineClients:      make(map[string]*myModule.Client, initClientNum),
		OnlineClientsSlice: make([]string, 0),
		OnlineClientsNum:   0,
	}
}

// 按照流程架構，不應會有同userName重複新增的問題
func (this *OnlineClientMgr) AddOnlineClient(client *myModule.Client) {
	this.OnlineClients[client.UserName] = client
	this.OnlineClientsSlice = append(this.OnlineClientsSlice, client.UserName)
	this.OnlineClientsNum++
}

func (this *OnlineClientMgr) DelOnlineClient(userName string) {
	if _, exists := this.OnlineClients[userName]; exists {
		delete(this.OnlineClients, userName)
		for index, value := range this.OnlineClientsSlice {
			if value == userName {
				len := len(this.OnlineClientsSlice)
				this.OnlineClientsSlice[index] = this.OnlineClientsSlice[len-1]
				this.OnlineClientsSlice = this.OnlineClientsSlice[:len-1]
				break
			}
		}
		this.OnlineClientsNum--
	}
}

func (this *OnlineClientMgr) SerializeSlice() ([]byte, error) {
	return json.Marshal(this.OnlineClientsSlice)
}

func DeserializeOnlineClientsSlice(data []byte) ([]string, error) {
	var onlineUsers []string
	err := json.Unmarshal(data, &onlineUsers)
	if err != nil {
		return nil, err
	}
	return onlineUsers, nil
}
