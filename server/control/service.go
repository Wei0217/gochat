package control

import (
	"errors"
	"fmt"
	_ "math/rand"
	"net"
	"strings"
	_ "time"

	"dennis.com/goChat/myModule"
)

// 處理各連線的業務
func HandleClientConnect(conn net.Conn) {
	defer func() {
		fmt.Println("HandleClientConnect err = ", conn.RemoteAddr(), "Close")
		conn.Close()
	}()
	fmt.Println(conn.RemoteAddr(), "已連線")

	// // produce random Guest token
	// rand.Seed(time.Now().UnixNano())
	// tempToken := myModule.MyToken(fmt.Sprintf("%v%v", "Guest", rand.Int()))

	// // send Token to client, every Message need take (temporary)token
	// msg := myModule.Message{
	// 	Token:     tempToken,
	// 	MsgType:   myModule.Msg_FirstConnect, // client第一次連接時，與下方Ack，client端定義方法更新token
	// 	ReqType:   myModule.Ack,
	// 	Content:   nil,
	// 	Timestamp: time.Now(),
	// }
	// myModule.SendMessageToRemote(&msg, conn)
	// fmt.Println("發送token至", conn.RemoteAddr())
	client := myModule.Client{
		Conn:     conn,
		UserId:   0,
		UserName: "Guest",
		Mode:     myModule.Mode_Guest,
		// Token:  tempToken,
		// Password: "Guest",
	}
	// // 加入map裡，方便管理
	// Clients[client.Token] = client

	// 持續監聽client端信息
	for {
		err := proccessCmd(&client)
		if err != nil {
			fmt.Println("HandleClientConnect proccessCmd err =", err)
			if strings.Contains(err.Error(), "wsarecv: An established connection was aborted by the software in your host machine.") {
				// 斷線error才跳出
				break
			}
			if strings.Contains(err.Error(), "wsarecv: An existing connection was forcibly closed by the remote host.") {
				// 斷線error才跳出
				break
			}
			if strings.Contains(err.Error(), "EOF") {
				break
			}
		}
	}
	// 若有問題，暫時先結束該client的連線
	fmt.Println("HandleClientConnect has something wrong.")
	// 關閉連線時，從Clients中移除
	MyOnlineClientMgr.DelOnlineClient(client.UserName)
	// delete(LoginClients, client.UserName)
}

func proccessCmd(client *myModule.Client) error {
	message, err := myModule.ReadMessageFromRemote(client.Conn) // 堵塞讀取信息
	if err != nil {
		fmt.Println("ReadMessageFromRemote err =", err)
		return err
	}

	// 判斷要進行何種業務
	switch message.MsgType {
	case myModule.Msg_UserLogin:
		switch message.ReqType {
		case myModule.Ack:
			err = UserLogin(message, client)
			if err != nil {
				fmt.Println("proccessCmd UserLogin err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_UserRegister:
		switch message.ReqType {
		case myModule.Ack:
			err = UserRegister(message, client)
			if err != nil {
				fmt.Println("proccessCmd UserRegister err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_GetOnlineUsers:
		switch message.ReqType {
		case myModule.Ack:
			err = GetOnlineUsers(message, client)
			if err != nil {
				fmt.Println("proccessCmd CreateSomeUser err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_Broadcast:
		switch message.ReqType {
		case myModule.Ack:
			err = BroadcastOnlineUser(message, client)
			if err != nil {
				fmt.Println("proccessCmd BroadcastOnlineUser err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_PrivateChat:
		switch message.ReqType {
		case myModule.Ack:
			err = PrivateChatOnlineUser(message, client)
			if err != nil {
				fmt.Println("proccessCmd PrivateChatOnlineUser err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_Exit:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_CreateSomeUser:
		switch message.ReqType {
		case myModule.Ack:
			err = CreateSomeUser(message, client)
			if err != nil {
				fmt.Println("proccessCmd CreateSomeUser err =", err)
				return err
			}
		case myModule.Req:
		case myModule.Nak:
		}

	default:
		fmt.Println("Unknown MsgType:", message.MsgType)
		return errors.New("Unknown Message")
	}
	return nil
}

func broadcastMessage(message string, sender *myModule.Client) {
	// for _, client := range Clients {
	// 	if client != sender {
	// 		client.Conn.Write([]byte(message))
	// 	}
	// }
}
