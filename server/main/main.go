package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"time"

	"dennis.com/goChat/model"
	"dennis.com/goChat/server/control"
)

// var Clients map[myModule.MyToken]myModule.Client
var (
	redisHost  = flag.String("redis", "localhost:6379", "The redis server")
	listenPort = flag.String("lport", ":8080", "Server listen port")
)

func init() {
	flag.Parse()
	fmt.Println("Chat server port is", *listenPort, ", Redis server is", *redisHost)
	// read configuration
	// exePath := myModule.GetExePath()
	// viper.SetConfigName("serverConf") // 配置文件的文件名，没有扩展名，如 .yaml, .toml 这样的扩展名
	// viper.SetConfigType("yaml")       // 设置扩展名。在这里设置文件的扩展名。另外，如果配置文件的名称没有扩展名，则需要配置这个选项
	// // viper.AddConfigPath("/etc/appname/") // 查找配置文件所在路径
	// // viper.AddConfigPath("$HOME/.appname") // 多次调用AddConfigPath，可以添加多个搜索路径
	// viper.AddConfigPath(exePath) // 还可以在工作目录中搜索配置文件
	// err := viper.ReadInConfig()  // 搜索并读取配置文件
	// if err != nil {              // 处理错误
	// 	panic(fmt.Errorf("Fatal error config file: %s \n", err))
	// }
	// redisHost = viper.GetString("redisHost")
	// listenPort = viper.GetString("listenPort")

	// 初始化 redis pool
	model.InitPool(*redisHost, 16, 0, 300*time.Second)
	model.InitUserDao()
	control.InitOnlineClientMgr(5)
	// control.LoginClients = make(map[string]*myModule.Client, 5)

}

func main() {
	listener, err := net.Listen("tcp", *listenPort)
	if err != nil {
		fmt.Println("Error starting the server:", err.Error())
		os.Exit(1)
	}
	defer listener.Close()

	fmt.Println("Server started. Listening on", *listenPort)

	for {
		// 堵塞，等待連線
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Accepting connection err =", err.Error())
			continue
		}

		// 每一個連線都是新的協程，分別處理各自的業務
		go control.HandleClientConnect(conn)
	}
}

// func handleClient(conn net.Conn) {
// 	defer func() {
// 		fmt.Println("handleClient err = ", conn.RemoteAddr(), "斷線")
// 		conn.Close()
// 	}()

// 	fmt.Println(conn.RemoteAddr(), "已連線")
// 	// produce random Guest token
// 	rand.Seed(time.Now().UnixNano())
// 	tempToken := myModule.CreateMyToken(fmt.Sprintf("%v%v", "Guest", rand.Int()))

// 	// 給Client發Token
// 	msg := myModule.Message{
// 		Token:     tempToken,
// 		MsgType:   myModule.Msg_FirstConnect,
// 		ReqType:   myModule.Ack,
// 		Content:   nil,
// 		Timestamp: time.Now(),
// 	}
// 	myModule.SendMessageToRemote(&msg, conn)
// 	fmt.Println("發送token至", conn.RemoteAddr())
// 	client := myModule.Client{
// 		Conn:     conn,
// 		Token:    tempToken,
// 		UserId:   "Guest",
// 		Password: "Guest",
// 		Name:     "Guest",
// 		Mode:     myModule.Mode_Guest,
// 	}
// 	Clients[client.Token] = client

// 	for {
// 		buffer := make([]byte, 1024)
// 		n, err := client.Conn.Read(buffer) // 賭塞
// 		if err != nil {
// 			fmt.Println("handleClient Read err =", err)
// 			return
// 		}
// 		fmt.Println("收到字節數: ", n)

// 		message, err := myModule.DeserializeMessage(buffer[:n])
// 		if err != nil {
// 			fmt.Println("handleClient DeserializeMessage err = ", err)
// 			continue
// 		}
// 		user, err := myModule.DeserializeUser(message.Content)
// 		if err != nil {
// 			fmt.Println("handleClient DeserializeUser err = ", err)
// 			continue
// 		}
// 		fmt.Println(user)
// 		fmt.Println("Server response:", user.UserName, user.Password)

// 		// 以上不斷接收client端送來的Message
// 		// 等待client端指令: 登入、註冊、退出
// 		switch client.Mode {
// 		case myModule.Mode_Guest:
// 		case myModule.Mode_Login:
// 		case myModule.Mode_PrivateChat:
// 		case myModule.Mode_MultiChat:
// 		default:
// 			fmt.Println("Unknown Message")
// 		}
// 	}
// }

// func broadcastMessage(message string, sender myModule.Client) {
// 	for _, client := range Clients {
// 		if client != sender {
// 			client.Conn.Write([]byte(message))
// 		}
// 	}
// }
