package control

import (
	"errors"
	"fmt"

	"dennis.com/goChat/model"
	"dennis.com/goChat/myModule"
	"dennis.com/goChat/server/control"
)

var ThisClient myModule.Client

// var TokenChan chan int8

func ProccessCmd(client myModule.Client) error {
	message, err := myModule.ReadMessageFromRemote(client.Conn) // 堵塞讀取信息
	if err != nil {
		fmt.Println("ProccessCmd ReadMessageFromRemote err =", err)
		return err
	}

	err = myModule.GoroutineCheckTimeout(message.TranId)
	if err != nil {
		fmt.Println("ProccessCmd GoroutineCheckTimeout err =", err)
		return err
	}

	// // tranId not match 表示主線程已進入下個循環，故不處理此timeout的message
	// if message.TranId != myModule.TranId {
	// 	return errors.New(fmt.Sprintf("Suppose tranId = %v, but %v", myModule.TranId, message.TranId))
	// }
	// select {
	// case myModule.ResTimeout <- 0: // 第一次send，讓main知道信息回來且沒超時，main等待第二次send的操作是否成功
	// 	fmt.Println("收到回應，未超時")
	// 	// 未超時所以繼續執行下方程式碼，必須再send一次 ResTimeout
	// case <-time.After(0): // 讓main知道信息回來且沒超時
	// 	fmt.Println("收到回應，已超時")
	// 	return errors.New("該回應已超時")
	// }

	switch message.MsgType {
	// case myModule.Msg_FirstConnect:
	// 	switch message.ReqType {
	// 	case myModule.Ack:
	// 		// // 剛與server連線時獲取token
	// 		// ThisClient.Token = message.Token
	// 		// myModule.ResTimeout <- 1 // 讓主線程不堵塞
	// 		// fmt.Println("連線成功...取得Token")
	// 	case myModule.Req:
	// 	case myModule.Nak:
	// 	}

	case myModule.Msg_UserLogin:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_UserLogin DeserializeResCode", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

			// server回傳交易成功
			if resCode.Code == myModule.Success {
				user, err := model.DeserializeUser(resCode.Body)
				if err != nil {
					fmt.Println("ProccessCmd Msg_UserLogin DeserializeUser 200", err)
					myModule.ResTimeout <- -1 // 讓主線程不堵塞
					return err
				}
				// 登入成功，更新本地client狀態
				ThisClient.UserId = user.UserId
				ThisClient.UserName = user.UserName
				ThisClient.Mode = myModule.Mode_Login
				fmt.Println("Login Success")
				myModule.ResTimeout <- 1 // 讓主線程不堵塞
				return nil

				// server回傳交易失敗
			} else if resCode.Code == myModule.UserLoginFail {
				fmt.Println("Login Fail")
				// 上面就有resCode了
				// resCode, err := myModule.DeserializeResCode(message.Content)
				// if err != nil {
				// 	fmt.Println("ProccessCmd Msg_UserLogin DeserializeResCode 300", err)
				// 	myModule.ResTimeout <- -1 // 讓主線程不堵塞
				// 	return err
				// }
				myModule.ResTimeout <- -1               // 讓主線程不堵塞
				return errors.New(string(resCode.Body)) // 錯誤信息
			}
		case myModule.Nak:
		}

	case myModule.Msg_UserRegister:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_UserRegister DeserializeResCode", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

			// server回傳交易成功
			if resCode.Code == myModule.Success {
				user, err := model.DeserializeUser(resCode.Body)
				if err != nil {
					fmt.Println("ProccessCmd Msg_UserRegister DeserializeUser 200", err)
					myModule.ResTimeout <- -1 // 讓主線程不堵塞
					return err
				}
				// 登入成功，更新本地client狀態
				ThisClient.UserId = user.UserId
				ThisClient.UserName = user.UserName
				ThisClient.Mode = myModule.Mode_Login
				fmt.Println("Register Success")
				myModule.ResTimeout <- 1 // 讓主線程不堵塞
				return nil

				// server回傳交易失敗
			} else if resCode.Code == myModule.UserRegisterFail {
				fmt.Println("Register Fail")
				// 上面就有resCode了
				// resCode, err := myModule.DeserializeResCode(message.Content)
				// if err != nil {
				// 	fmt.Println("ProccessCmd Msg_UserLogin DeserializeResCode 300", err)
				// 	myModule.ResTimeout <- -1 // 讓主線程不堵塞
				// 	return err
				// }
				myModule.ResTimeout <- -1               // 讓主線程不堵塞
				return errors.New(string(resCode.Body)) // 錯誤信息
			}
		case myModule.Nak:
		}

	case myModule.Msg_CreateSomeUser:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_CreateSomeUser DeserializeResCode", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

			// server回傳交易成功
			if resCode.Code == myModule.Success {
				fmt.Println("CreateSomeUser Success")
				myModule.ResTimeout <- 1 // 讓主線程不堵塞
				return nil
			} else if resCode.Code == myModule.CreateSomeUserFail { // server回傳交易失敗
				fmt.Println("CreateSomeUser Fail")
				myModule.ResTimeout <- -1               // 讓主線程不堵塞
				return errors.New(string(resCode.Body)) // 錯誤信息
			}
		case myModule.Nak:
		}

	case myModule.Msg_GetOnlineUsers:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_GetOnlineUsers DeserializeResCode", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}
			fmt.Println("Msg_GetOnlineUsers Success")
			onlineUsers, err := control.DeserializeOnlineClientsSlice(resCode.Body)
			if err != nil {
				fmt.Println("Msg_GetOnlineUsers DeserializeOnlineClientsSlice err =", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

			fmt.Println()
			fmt.Println("-----Online Users-----")
			for i, value := range onlineUsers {
				fmt.Printf("No. %v user = %v\n", i, value)
			}
			fmt.Println("----------------------")
			fmt.Println()

			myModule.ResTimeout <- 1 // 讓主線程不堵塞
			return nil
		case myModule.Nak:
		}

	case myModule.Msg_Broadcast:
		switch message.ReqType {
		case myModule.Ack:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_Broadcast DeserializeResCode", err)
				return err
			}
			fmt.Printf("\nBroadcast: \n%q\n", string(resCode.Body))
			return nil
		case myModule.Req:
		case myModule.Nak:
		}

	case myModule.Msg_PrivateChat:
		switch message.ReqType {
		case myModule.Ack:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_PrivateChat Ack DeserializeResCode", err)
				return err
			}
			fmt.Printf("\nPrivateChat: \n%q\n", string(resCode.Body))
			return nil

		case myModule.Req:
			resCode, err := myModule.DeserializeResCode(message.Content)
			if err != nil {
				fmt.Println("ProccessCmd Msg_PrivateChat Req DeserializeResCode", err)
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

			if resCode.Code == myModule.Success {
				fmt.Println("Private Chat Send Success")
				myModule.ResTimeout <- 1 // 讓主線程不堵塞
				return nil
			} else if resCode.Code == myModule.PrivateChatFail {
				fmt.Println(string(resCode.Body))
				myModule.ResTimeout <- -1 // 讓主線程不堵塞
				return err
			}

		case myModule.Nak:
		}

	case myModule.Msg_Exit:
		switch message.ReqType {
		case myModule.Ack:
		case myModule.Req:
		case myModule.Nak:
		}

	default:
		fmt.Println("Unknown MsgType:", message.MsgType)
		myModule.ResTimeout <- -1 // 讓主線程不堵塞
		return errors.New("Unknown Message")
	}
	return nil
}
