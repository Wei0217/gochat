package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	"dennis.com/goChat/client/control"
	"dennis.com/goChat/myModule"
	"github.com/spf13/viper"
)

var (
	exit       bool
	err        error
	serverHost string
)

func init() {
	// read configuration
	exePath := myModule.GetExePath()
	viper.SetConfigName("clientConf") // 配置文件的文件名，没有扩展名，如 .yaml, .toml 这样的扩展名
	viper.SetConfigType("yaml")       // 设置扩展名。在这里设置文件的扩展名。另外，如果配置文件的名称没有扩展名，则需要配置这个选项
	// viper.AddConfigPath("/etc/appname/") // 查找配置文件所在路径
	// viper.AddConfigPath("$HOME/.appname") // 多次调用AddConfigPath，可以添加多个搜索路径
	viper.AddConfigPath(exePath) // 还可以在工作目录中搜索配置文件
	err := viper.ReadInConfig()  // 搜索并读取配置文件
	if err != nil {              // 处理错误
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	serverHost = viper.GetString("serverHost")
	// hacker := viper.GetBool("Hacker")
	// name := viper.GetString("name")
	// hobbies := viper.GetStringSlice("hobbies")
	// jacket := viper.Get("clothing.jacket")
	// age := viper.GetInt("age")
	// fmt.Println("name: ", name, "Hacker: ", hacker, ",hobbies: ", hobbies, ",jacket: ", jacket, ",age: ", age)

	control.ThisClient = myModule.Client{
		Conn:     nil,
		UserId:   0,
		UserName: "Guest",
		Mode:     myModule.Mode_Guest,
		// Token:  myModule.MyToken("Guest"),
		// Password: "Guest",
	}
	myModule.TranId = 1 // 從1開始，0有特殊意義
	myModule.ResTimeout = make(chan int8, 0)
	err = nil
	// myModule.ResTimeout = make(chan bool, 0)
	// control.TokenChan = make(chan int8, 0)
	// control.WaitTran = false

}

// hset users mike "{\"userId\":200,\"userName\":\"mike\",\"userPwd\":\"456\"}"
func main() {
	fmt.Println("連線中...server:", serverHost)
	// 沒連上的話，一直連
	for {
		control.ThisClient.Conn, err = net.Dial("tcp", serverHost)
		if err != nil {
			fmt.Println("net.Dial err =", err)
			err = nil
			time.Sleep(time.Second)
			continue
		}
		break
	}
	defer func() {
		fmt.Println(control.ThisClient.Conn.RemoteAddr(), "close")
		control.ThisClient.Conn.Close()
		close(myModule.ResTimeout)
		// close(control.TokenChan)
		// close(control.LoginChan)
	}()

	// get token from server
	// buffer := make([]byte, 1024)
	// n, err := control.ThisClient.Conn.Read(buffer) // 賭塞
	// if err != nil {
	// 	log.Println("get token Read err =", err)
	// 	return
	// }

	// // 處理收到的token
	// message, err := myModule.DeserializeMessage(buffer[:n])

	go func() {
		for {
			err := control.ProccessCmd(control.ThisClient) // 堵塞讀取信息，處理cmd
			if err != nil {
				if strings.Contains(err.Error(), "wsarecv: An established connection was aborted by the software in your host machine.") {
					// 斷線error才跳出
					break
				}
				if strings.Contains(err.Error(), "wsarecv: An existing connection was forcibly closed by the remote host.") {
					// 斷線error才跳出
					break
				}
				if strings.Contains(err.Error(), "EOF") {
					break
				}
				fmt.Println("ProccessCmd err =", err)
			}
			// 斷線以外的error 及 正常情況，會持續下一輪讀取
		}
		fmt.Println("斷線")
		os.Exit(0)
	}()

	// fmt.Println("等待server回應")
	// // <-control.TokenChan // 堵塞等待server給token
	// err := myModule.MainCheckTimeout(5)
	// if err != nil {
	// 	fmt.Println("Get Token Error =", err)
	// 	return
	// }
	// time.Sleep(1 * time.Second)

	for {
		// ClientModeService()
		switch control.ThisClient.Mode {
		case myModule.Mode_Guest:
			// 使用者未登入的介面
			GuestBusiness()
		case myModule.Mode_Login:
			// 使用者登入後的介面
			LoginBusiness()
		case myModule.Mode_PrivateChat:
			// 使用者登入後的使用私聊的介面
			PrivateChatBusiness()
		case myModule.Mode_MultiChat:
			// 使用者登入後的使用群聊的介面
			MultiChatBusiness()
		}
		if exit {
			break
		}
	}

}

// 讀取服務器回應
// func ListenServer() {
// 	buffer := make([]byte, 1024)
// 	for {
// 		n, err := control.ThisClient.Conn.Read(buffer)
// 		if err != nil {
// 			log.Println("ListenServer Read err =", err)
// 			return
// 		}

// 		// 處理服務器回應
// 		fmt.Println("Server response:", string(buffer[:n]))
// 		message, err := myModule.DeserializeMessage(buffer[:n])
// 		control.ProccessCmd(message)
// 	}
// }

// func ProccessCmd(message *myModule.Message) {
// 	msg := *message
// 	switch msg.MsgType {
// 	case myModule.Msg_FirstConnect:
// 		switch msg.ReqType {
// 		case myModule.Ack:
// 			// 剛與server連線時獲取token
// 			ThisClient.Token = msg.Token
// 			fmt.Println(ThisClient)
// 			fmt.Println("連線成功...取得Token")
// 		case myModule.Req:
// 		case myModule.Nak:
// 		}
// 	case myModule.Msg_UserLogin:
// 		switch msg.ReqType {
// 		case myModule.Ack:
// 		case myModule.Req:
// 		case myModule.Nak:
// 		}
// 	case myModule.Msg_UserCreate:
// 		switch msg.ReqType {
// 		case myModule.Ack:
// 		case myModule.Req:
// 		case myModule.Nak:
// 		}
// 	case myModule.Msg_Exit:
// 		switch msg.ReqType {
// 		case myModule.Ack:
// 		case myModule.Req:
// 		case myModule.Nak:
// 		}
// 	default:
// 		fmt.Println("Unknown Message")
// 	}
// }

func GuestBusiness() {
	// 在這裡處理用戶與服務器的互動邏輯，例如登入、註冊、發送訊息等
	fmt.Println("\n----------Go Chat----------")
	fmt.Println("       (1) 用戶登入")
	fmt.Println("       (2) 用戶註冊")
	fmt.Println("       (3) 退出程序")

	// 提示用戶輸入訊息
	fmt.Print("Enter command(number), press Enter: ")
	// 以下是一個示例的互動邏輯
	reader := bufio.NewReader(os.Stdin)
	cmd, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("GuestBusiness ReadString err =", err)
		return
	}
	cmd = myModule.TrimNewline(cmd)
	fmt.Printf("%q\n", cmd)

	switch cmd {
	case "1":
		err := control.ThisClient.Login()
		if err != nil {
			fmt.Println("GuestBusiness 1 Login err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 堵塞，直到成功，或5秒timeout
		err = myModule.MainCheckTimeout(5)
		if err != nil {
			fmt.Println("GuestBusiness 1 MainCheckTimeout err =", err)
		}
		time.Sleep(500 * time.Millisecond)

	case "2":
		err := control.ThisClient.Register()
		if err != nil {
			fmt.Println("GuestBusiness 2 Register err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 堵塞，直到成功，或5秒timeout
		err = myModule.MainCheckTimeout(5)
		if err != nil {
			fmt.Println("GuestBusiness 2 MainCheckTimeout err =", err)
		}
		time.Sleep(500 * time.Millisecond)

	case "3":
		fmt.Println("退出")
		exit = true

	case "404":
		err := control.ThisClient.CreateSomeUsers()
		if err != nil {
			fmt.Println("GuestBusiness 404 CreateSomeUsers err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 堵塞，直到成功，或5秒timeout
		err = myModule.MainCheckTimeout(5)
		if err != nil {
			fmt.Println("GuestBusiness 404 MainCheckTimeout err =", err)
		}
		time.Sleep(500 * time.Millisecond)

	default:
		fmt.Println("無效指令，請重新輸入")
	}

}

func LoginBusiness() {
	// 登入成功後的使用者，顯示其他功能介面
	fmt.Println("\n----------Go Chat----------")
	fmt.Println("User:", control.ThisClient.UserName)
	fmt.Println("   (1) 顯示在線用戶列表")
	fmt.Println("   (2) 私人聊天")
	fmt.Println("   (3) 多人聊天")
	fmt.Println("   (4) 退出程序")

	// 提示用戶輸入訊息
	fmt.Print("Enter command(number), press Enter: ")
	// 以下是一個示例的互動邏輯
	reader := bufio.NewReader(os.Stdin)
	cmd, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("GuestBusiness ReadString err =", err)
		return
	}
	cmd = myModule.TrimNewline(cmd)
	// fmt.Printf("%q\n", cmd)

	switch cmd {
	case "1":
		err := control.ThisClient.GetOnlineUsers()
		if err != nil {
			fmt.Println("LoginBusiness 1 GetOnlineUsers err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 堵塞，直到成功，或5秒timeout
		err = myModule.MainCheckTimeout(5)
		if err != nil {
			fmt.Println("LoginBusiness 1 MainCheckTimeout err =", err)
		}
		time.Sleep(500 * time.Millisecond)

	case "2":
		fmt.Println("私人聊天")
		err := control.ThisClient.PrivateChat()
		if err != nil {
			fmt.Println("LoginBusiness 2 PrivateChat err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 堵塞，直到成功，或5秒timeout
		err = myModule.MainCheckTimeout(5)
		if err != nil {
			fmt.Println("LoginBusiness 2 MainCheckTimeout err =", err)
		}
		time.Sleep(500 * time.Millisecond)

	case "3":
		fmt.Println("多人聊天")
		err := control.ThisClient.Broadcast()
		if err != nil {
			fmt.Println("LoginBusiness 3 Broadcast err =", err)
			myModule.UpdateTranId() // 以防萬一，直接丟棄這輪的response
			return
		}

		// 這指令不等回應，會群發一個trandId=0的message給每個OnlineUser
		// // 堵塞，直到成功，或5秒timeout
		// err = myModule.MainCheckTimeout(5)
		// if err != nil {
		// 	fmt.Println("LoginBusiness 3 Broadcast err =", err)
		// }
		// time.Sleep(500 * time.Millisecond)

	case "4":
		fmt.Println("退出")
		exit = true

	default:
		fmt.Println("無效指令，請重新輸入")
	}
}

func PrivateChatBusiness() {
	fmt.Println("PrivateChatBusiness")
}

func MultiChatBusiness() {
	fmt.Println("MultiChatBusiness")
}

func selectTest() {
	// continue會直接重新for
	// 注意!!! break只會跳出select{}
	// return 跳出func

	// tick := time.Tick(time.Second)
	// for {
	// 	fmt.Println(123)
	// 	select {
	// 	case t := <-tick:
	// 		fmt.Println(t)
	// 		return
	// 		fmt.Println("test")
	// 	}
	// 	fmt.Println(456)
	// }

	// select完不會重複迴圈select代碼，繼續往下執行
	// ch := make(chan int, 1)
	// ch <- 123
	// select {
	// case t := <-ch:
	// 	fmt.Println(t)
	// }
	// fmt.Println(456)

	// close(control.LoginTimeout)
	// go func() {
	// 	time.Sleep(3 * time.Second)
	// 	fmt.Println("time out")
	// 	control.LoginTimeout <- false
	// 	fmt.Println("end go")
	// }()
	// var a string
	// fmt.Scanf("%v\n", &a)
	// if a == "1" {
	// 	control.LoginTimeout = make(chan bool, 0)
	// 	<-control.LoginTimeout
	// 	fmt.Println(123)
	// }
	// close(control.LoginTimeout)
	// fmt.Printf("%q\n", a)

	// timeout機制
	timeout := 2 * time.Second
	timer := time.NewTimer(timeout)
	count := 0
	for {
		count++
		done := make(chan bool)

		go func() {
			// 做xx秒工作測試
			time.Sleep(1995 * time.Millisecond)
			select {
			case done <- true:
				fmt.Printf("Task completed goroutine %v\n", done)
			case <-time.After(0):
				fmt.Printf("超時 goroutine %v\n", done)
			}
			// 自己關掉，不然for愈來愈多
			close(done)
		}()

		select {
		case <-timer.C:
			fmt.Printf("超時 timer.C %v\n", done)
		case <-done:
			fmt.Printf("Task completed main %v\n", done)
			<-timer.C // 清空計時器的通道
			timer.Stop()
			fmt.Println(count)
			return
		}
		// close(done)
		fmt.Println(787)
		// 重置計時器，準備下一輪計時
		timer.Reset(timeout)
	}

}
